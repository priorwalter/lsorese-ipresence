/**
 * es6 support
 */
import 'unfetch/polyfill';
import 'es6-promise/auto';

/**
 * modules
 */
import './layouts/theme';