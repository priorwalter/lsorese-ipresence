import PostObject from '../modules/PostObject';
import Popup from '../modules/Popup';

export default function Cards(card, token) {
  // Assign listener
  card.addEventListener('click', (card) => {
    // Fetch data
    var handle = card.currentTarget.getAttribute('data-item');
    fetch(`https://taproom-lsorese.myshopify.com/api/graphql`, PostObject(handle, token))
      .then(res => res.json())
      .then(res => {
        // Get product
        var product = res.data.productByHandle;
        Popup(product);

      });
  });
}