import Glide from '@glidejs/glide';

export default function Slideshow() {
  // Featured
  if (document.querySelector('.featured')) {
    new Glide('.featured', {
      type: 'carousel',
      autoplay: 10000,
      animationDuration: 600,
      perView: 4,
      gap: 0,
      breakpoints: {
        1151: {
          perView: 3,
        },
        1023: {
          perView: 2,
        },
        767: {
          perView: 1,
          peek: '40%',
        }
      }
    }).mount();
  }
  if (document.querySelector('.hero')) {
    // Hero
    new Glide('.hero', {
      type: 'carousel',
      autoplay: 9000,
      gap: 0,
      animationDuration: 600,
      perView: 1
    }).mount();

  }
}