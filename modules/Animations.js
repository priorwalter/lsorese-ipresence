export default function Animations() {
  // Plus/Minus
  var count = 1;
  var countEl = document.getElementById("popup__quantity");
  var minusClick = document.querySelector(".button__minus");
  var plusClick = document.querySelector(".button__plus");

  plusClick.addEventListener('click', () => {
    count++;
    countEl.value = count;
  });
  minusClick.addEventListener('click', () => {
    if (count > 1) {
      count--;
      countEl.value = count;
    }
  });

  // Menu
  var ham = document.querySelector('.hamburger');
  var hamButton = document.querySelector('.header__hamburger');
  var menuTop = document.querySelector('.menu__top');
  var menuBottom = document.querySelector('.menu__bottom');

  hamButton.addEventListener('click', () => {
    ham.classList.toggle('open');
    menuTop.classList.toggle('open');
    menuBottom.classList.toggle('open');
    body.classList.toggle('locked');
  });

  // Popup
  var body = document.querySelector('.template-index');
  var popup = document.querySelector('.popup');
  var popupClose = document.querySelector('.popup__close');

  popupClose.addEventListener('click', () => {
    popup.classList.toggle('open');
    body.classList.toggle('locked');

  })
}