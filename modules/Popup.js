export default function Popup(product) {

  // Get elements
  var popup = document.querySelector('.popup');
  var body = document.querySelector('.template-index');
  var popupImage = document.querySelector('.popup__image');
  var popupProduct = document.querySelector('.popup__product');
  var addToCartButton = document.querySelector('.add-to-cart');

  var item = product;
  console.log([
    "Fetch a Product:",
    product
  ]);
  var price, description, sku, image, vendor, title = "";
  if (item !== undefined) {
    var title = item.title;
    var vendor = item.vendor;
    var price = item.variants.edges[0].node.price;
    var description = item.description;
    var sku = item.variants.edges[0].node.sku;
    var image = item.images.edges[0].node.src;
    var id = item.variants.edges[0].node.id;
  }
  // Some regex oddness to get the ID correct.
  // Look for better way.

  // Markup for popup
  var imageTag = `<img src="${image}" />`;
  var copy = `
      <h3>${vendor}</h3>
      <h4 class="sku">${sku}</h4>
      <h2>${vendor}</h2>
      <hr/>
      <div class="price">$${price}</div>
      <p class="big">${description}</p>
      <hr/>
  `;

  popupImage.innerHTML = imageTag;
  popupProduct.innerHTML = copy;
  addToCartButton.setAttribute('data-id', id);

  // Show popup
  if (item !== undefined) {
    popup.classList.toggle('open');
    body.classList.toggle('locked');
  }
}