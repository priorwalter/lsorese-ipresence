export default function PostObject(handle, token) {
  return {
    method: "post",
    headers: {
      "Content-Type": "application/graphql",
      "X-Shopify-Storefront-Access-Token": token
    },
    body: `{
      productByHandle(handle:"${handle}") {
        id
        title
        description
        vendor
        variants(first: 5) {
          edges {
            node {
              price
              sku
              id
             }
           }
        }
        priceRange {
          minVariantPrice {
            amount
          }
          maxVariantPrice {
            amount
          }
        }
        images(first: 5) {
         edges {
           node {
             src
           }
         }
       }
      }
    }`
  }
};