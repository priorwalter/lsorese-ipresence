import * as cart from '@shopify/theme-cart';

export default function AddToCart(id, quantity) {

  // From GraphQL to Cart, we get a base64 encoded response.
  // Almost certainly a hack.
  var idDecoded = atob(id);
  var productID = parseInt(idDecoded.substr(idDecoded.lastIndexOf('/') + 1));

  cart.addItem(productID, {
    quantity
  }).then(item => {
    alert(
      `An item with a quantity of ${quantity} was added to your cart. Posted info in console.`
    );
    console.log([
      "Added to Cart:",
      item
    ]);
  });

}