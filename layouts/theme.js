import Cards from '../modules/Cards';
import AddToCart from '../modules/AddToCart'
import Slideshow from '../modules/Slideshow';
import Animations from '../modules/Animations';
const token = "e7dfc8cfafbc1f2527693857f078cb72";
Animations();
Slideshow();

// For all quick-view items, generate an event for each.
var cards = document.querySelectorAll('.quick');
cards.forEach((card) => {
  Cards(card, token);
});

// Cart button to buy the item.
var addItem = document.querySelector('.add-to-cart');
addItem.addEventListener('click', (card) => {
  var id = document.querySelector('.add-to-cart').getAttribute('data-id');
  var quantity = parseInt(document.getElementById('popup__quantity').value);
  AddToCart(id, quantity);
})